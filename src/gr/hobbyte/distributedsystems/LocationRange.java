/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */

package gr.hobbyte.distributedsystems;

import java.io.Serializable;

public class LocationRange implements Serializable {

    private static final long serialVersionUID = 12345678905L;

    private double startLong, startLat, endLong, endLat;

    public LocationRange(double startLong, double startLat, double endLong, double endLat ){
        super();
        this.startLong = startLong;
        this.startLat = startLat;
        this.endLong = endLong;
        this.endLat = endLat;
    }

    public double getStartLong(){
        return startLong;
    }

    public void setStartLong(double startLong){
        this.startLong = startLong;
    }

    public double getStartLat(){
        return startLat;
    }

    public void setStartLat(double startLat){
        this.startLat = startLat;
    }

    public double getEndLong(){
        return endLong;
    }

    public void setEndLong(double endLong){
        this.endLong = endLong;
    }

    public double getEndLat(){
        return endLat;
    }

    public void setEndLat(double endLat){
        this.endLat = endLat;
    }

    public String toString(){
        return "Starting point: " + startLong + ", " + startLat + "\nEnding point: " + endLong + ", " + endLat;
    }

}
