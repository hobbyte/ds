/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */

package gr.hobbyte.distributedsystems;

import java.io.Serializable;
import java.util.Date;

/**
 * This object holds information about a "check-in" db row.
 */
class POIs implements Serializable, Cloneable {

    private static final long serialVersionUID = 12345678905L;

    private String POI, name, cat;
    private double lat, lon;
    private int counter;
    Date time;

    POIs(String POI, String name, String cat, double lat, double lon, int counter, Date time){
        super();
        this.POI = POI;
        this.name = name;
        this.cat = cat;
        this.lat = lat;
        this.lon = lon;
        this.counter = counter;
        this.time = time;
    }

    public String getPOI() {
        return POI;
    }

    public void setPOI(String POI) {
        this.POI = POI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    @Override
    public int hashCode() {
        return this.POI.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof POIs))return false;
        POIs tocheck = ((POIs) obj);
        return this.POI.equals(tocheck.getPOI());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new POIs(POI, name, cat, lat, lon, counter, (Date) time.clone());
    }
}
