/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */
package gr.hobbyte.distributedsystems;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class MapWorker extends Worker implements Runnable{

    private Socket connection;
    private ObjectOutputStream out;
    ObjectInputStream in;
    private String client_ip;
    private ArrayList<Checkins> buffer;

    private String reducer_ip = "127.0.0.1";//reducer ip address
    private int reducer_port = 2485;// reducer port

    private int mode, rand; //mapper name

    private int topK;

    public MapWorker(Socket ss, int mode, ArrayList<Checkins> buf, int rand){
        this.connection = ss;
        this.mode = mode;
        this.buffer = buf;
        this.rand = rand;
    }

    public void run(){
        openServer();
    }


    /**
     * The method used to map the check ins
     * @param time regards the time frame for the query
     * @param area regards the area limitation of the query
     * @return a list of pairs {@code Map<Integer, Object>} of POIs and their check in counter
     */
    private Map<String, Checkins> map(DateRange time, LocationRange area){
        /**
         * The Map<> we save the database's results and is returned at the end.
         * @param Integer will be used for check in counter
         * @param Object will be used for the POI
         */
        System.out.println("Mapper_" + Integer.toString(mode) + ": Processing data...");
        Map<String, Checkins> mapper2 = buffer.stream()
                .filter(e ->  e.getPOI().getLat()>area.getStartLat()&& e.getPOI().getLat() < area.getEndLat()&&e.getPOI().getLon()>area.getStartLong()&& e.getPOI().getLon() < area.getEndLong() && time.getStartDate().before(e.getPOI().getTime())  && time.getEndDate().after(e.getPOI().getTime()))
                .map(p -> {
                    Map.Entry<String, Checkins> firstEntry = new AbstractMap.SimpleEntry<>(p.getPOI().getPOI(), p);
                    return firstEntry;
                })
                .collect(HashMap<String, Checkins>::new,
                        (map, str) ->{
                            if(!map.containsKey(str.getKey())){
                                map.put(str.getKey(),str.getValue());
                            }else{
                                str.getValue().addPhotos(map.get(str.getKey()).getPhotos());
                                str.getValue().getPOI().setCounter(map.get(str.getKey()).getPOI().getCounter()+1);
                                map.put(str.getKey(),str.getValue());
                            }
                        },
                        HashMap<String, Checkins>::putAll);

        return mapper2;
    }

    private void notifyMaster(){

    }

    /**
     * Sends results from Map function to the reducer
     * @param map
     */
    private void sentToReducers(Map<String, Checkins> map){
        boolean scan = true;
        while(scan) {
            try {
                Socket s = new Socket(InetAddress.getByName(reducer_ip), rand);
                scan = false; //if can't connect will retry every 2 seconds.
                OutputStream os = s.getOutputStream();
                InputStream is = s.getInputStream();
                ObjectOutputStream oos = new ObjectOutputStream(os);
                ObjectInputStream ois = new ObjectInputStream(is);
                oos.writeObject(client_ip);
                oos.writeInt(rand);
                oos.flush();
                int c = ois.readInt();
                if(c==1) {
                    oos.writeObject(map);
                    oos.writeInt(topK);
                    oos.flush();
                }
                oos.close();
                os.close();
                s.close();
                if(c==1)
                    return;
            } catch (IOException ioException) {
                System.out.println("Mapper_" + Integer.toString(mode) + ": Connection to reducer failed. Re-trying...");
                try {
                    Thread.sleep(2000); //if failed, wait and retry
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ioException.printStackTrace();
            }
        }
    }

    /**
     * Starts Map worker server.
     */
    void openServer(){
        try {
            System.out.println("Mapper connected");
            DateRange dr;
            LocationRange lr;
            InetAddress ina = connection.getInetAddress();
            client_ip = ina.toString().substring(1);
            out = new ObjectOutputStream(connection.getOutputStream());
            in = new ObjectInputStream(connection.getInputStream());
            out.writeObject("Mapper_" + Integer.toString(mode) + ": Connection successful!");
            out.flush();
            try{
                Date start_date = (Date) in.readObject();
                Date end_date = (Date) in.readObject();
                dr = new DateRange(start_date, end_date);
                double sLong, sLat, eLong, eLat;
                sLong = in.readDouble();
                eLat = in.readDouble();
                eLong = in.readDouble();
                sLat = in.readDouble();
                System.out.println(eLat+" "+sLat);
                lr = new LocationRange(sLong, sLat, eLong, eLat);
                topK = in.readInt();
                Map<String, Checkins> mapped = map(dr, lr);
                sentToReducers(mapped);
            } catch (ClassNotFoundException classnot){
                out.writeObject("Mapper_" + Integer.toString(mode) + ": Unknown format.");
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();

        } finally {
            try {
                out.close();
                in.close();
                connection.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }



    }
}