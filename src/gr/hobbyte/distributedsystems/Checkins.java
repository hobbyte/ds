/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */

package gr.hobbyte.distributedsystems;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This object holds a POIs object and
 * an ArrayList to store all photos
 */
public class Checkins implements Serializable, Cloneable{
    private POIs POI;
    private ArrayList<String> photos;
    private static final long serialVersionUID = 12345678904L;
    Checkins(POIs POI, ArrayList<String> photos){
        this.POI = POI;
        this.photos = photos;
    }

    public POIs getPOI() {
        return POI;
    }

    public void setPOI(POIs POI) {
        this.POI = POI;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public void addPhotos(ArrayList<String> photos){
        this.photos.addAll(photos);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ArrayList<String> newPhotos = new ArrayList<>(photos.size());
        for(String str : photos){
            newPhotos.add(str);
        }
        return new Checkins((POIs) POI.clone(), newPhotos);
    }
}
