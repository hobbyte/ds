/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */

package gr.hobbyte.distributedsystems;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

public class ReduceWorker extends Worker implements Runnable{
    private ServerSocket serverSocket;
    private Socket soc;
    Socket toSend;
    OutputStream os = null;
    ObjectOutputStream oos;
    private int rand = 0;
    private Map<String, Checkins> map1, map2, map3;
    private String client_ip = null;
    private int client_port = 1547; //why not?
    int limit = 10;

    public ReduceWorker(ServerSocket ss, int rand){
        this.serverSocket = ss;
        this.rand = rand;
    }
    public void run(){
        waitServer();
    }

    public void waitServer(){
        try {
            ArrayList<LinkedHashMap<String, Checkins>> mx = new ArrayList<>();
            System.out.println("Reducer: I'm online.");
            //After receiving three mappers we should wait for another request
            for (int objectsWait = 0; objectsWait < 3;) {
                soc = serverSocket.accept();//wait for a mapper
                InputStream is = soc.getInputStream();
                OutputStream os = soc.getOutputStream();
                ObjectInputStream ois = new ObjectInputStream(is);
                ObjectOutputStream oos = new ObjectOutputStream(os);
                String temp_client_ip;
                //read map objects
                if (objectsWait == 0) {
                    client_ip = (String) ois.readObject();
                    int temp_rand = ois.readInt();
                    if(rand!=temp_rand){
                        oos.writeInt(0);
                        oos.flush();
                        continue;
                    }
                    else {
                        oos.writeInt(1);
                        oos.flush();
                        objectsWait++;
                    }
                    map1 = (Map<String, Checkins>) ois.readObject();
                    limit = ois.readInt();
                    System.out.println("Reducer: Got mapper(with reducerKey=" + objectsWait + ") of size: " + map1.size());
                } else if (objectsWait == 1) {
                    temp_client_ip = (String) ois.readObject();
                    int temp_rand = ois.readInt();
                    if(!temp_client_ip.equals(client_ip)||rand!=temp_rand){
                        oos.writeInt(2);
                        oos.flush();
                        continue;
                    }
                    else {
                        oos.writeInt(1);
                        oos.flush();
                        objectsWait++;
                    }
                    map2 = (Map<String, Checkins>) ois.readObject();
                    limit = ois.readInt();
                    System.out.println("Reducer: Got mapper(with reducerKey=" + objectsWait + ") of size: " + map2.size());
                } else {
                    temp_client_ip = (String) ois.readObject();
                    int temp_rand = ois.readInt();
                    if(!temp_client_ip.equals(client_ip)||rand!=temp_rand){
                        oos.writeInt(3);
                        oos.flush();
                        continue;
                    }
                    else {
                        oos.writeInt(1);
                        oos.flush();
                        objectsWait++;
                    }
                    map3 = (Map<String, Checkins>) ois.readObject();
                    limit = ois.readInt();
                    System.out.println("Reducer: Got mapper(with reducerKey=" + objectsWait + ") of size: " + map3.size());
                }
            }
            mx.add(new LinkedHashMap<>(map1));
            mx.add(new LinkedHashMap<>(map2));
            mx.add(new LinkedHashMap<>(map3));
            sendResults(reduce(mx)); //perform the reduce function TODO: If error => Check for complications on Map <==> LinkedHashMap.
            oos.close();
            os.close();
            toSend.close();
            System.out.println("Reducer: Completed job.");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                soc.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

    }

    public void waitForMasterAck(){

    }

    /**
     * Combines the three maps into 1 called mx.
     * @return LinkedHashMap with results
     */
    public Map<String, Checkins> reduce(ArrayList<LinkedHashMap<String, Checkins>> mx){
        LinkedHashMap<String, Checkins> result = new LinkedHashMap<>();
        result = mx.parallelStream().reduce((mapToReturn,entryMap) ->  {
            mapToReturn.putAll(entryMap);
            return mapToReturn;
        }).get();
        result = result.entrySet().parallelStream()
                .sorted(Comparator.comparingInt(e -> e.getValue().getPOI().getCounter()))
                .collect(LinkedHashMap<String, Checkins>::new,
                        (map, str) ->{
                            ArrayList<String> al = new ArrayList<>();
                            Set<String> hs = new HashSet<>();
                            hs.addAll(str.getValue().getPhotos());
                            al.addAll(hs);
                            str.getValue().setPhotos(al);
                            map.put(str.getKey(),str.getValue());
                        },
                        LinkedHashMap<String, Checkins>::putAll);

        LinkedHashMap<String, Checkins> finalResult = new LinkedHashMap<>();

        ListIterator<Map.Entry<String, Checkins>> iterator = new ArrayList<>(result.entrySet()).listIterator(result.size());
        while (iterator.hasPrevious()){
            Map.Entry<String, Checkins> entry = iterator.previous();
            finalResult.put(entry.getKey(), entry.getValue());
        }
        return finalResult;
    }

    /**
     * Prints worker results just for debugging
     * @return
     */
     public Object sendResults(Map<String, Checkins> map){


         if(map != null) {
             int max_tries = 3; //we need to specify number of tries to resolve any network errors
             int tries = 0;
             boolean scan = true;
             int c = 0;

             while(scan && tries <= max_tries) {
                 try {
                     toSend = new Socket(InetAddress.getByName(client_ip), client_port);
                     System.out.println("Reducer is ready to transmit data...");
                     scan = false; //if can't connect will retry every 5 seconds for max_tries times.
                     os = toSend.getOutputStream();
                     oos = new ObjectOutputStream(os);
                 } catch (IOException e) {
                     System.out.println("Reducer: Can't connect to client at " + client_ip + ":" + client_port);
                     tries++;
                     try {
                         Thread.sleep(5000); //if failed, wait and retry
                     } catch (InterruptedException ex) {
                         ex.printStackTrace();
                     }
                 }
             }
             for (Map.Entry e : map.entrySet()) {
                 if(c<limit){
                     try {
                         oos.writeObject(e.getValue());
                         oos.flush(); //send

                     } catch (IOException ioException) {
                         System.out.println("Reducer: Can't connect to client at " + client_ip + ":" + client_port);
                         try {
                             Thread.sleep(5000); //if failed, wait and retry
                         } catch (InterruptedException ex) {
                             ex.printStackTrace();
                         }
                         ioException.printStackTrace();
                     }

                     c++;
                 }
             }
         }else{
             System.out.println("NULL MAP!");
         }

         return null;
    }
}
