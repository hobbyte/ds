/*
Athens University of Economics and Business
Course: Distributed Systems

David Angelos (3130049)
Giotas Konstantinos (3130040)
Gketsoppoulos Petros (3130041)
 */

package gr.hobbyte.distributedsystems;

import com.sun.corba.se.pept.encoding.InputObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Worker {
    ArrayList<Checkins> buffer;
    ArrayList<Checkins> tempBuffer;
    public static void main(String [ ] args){
        Worker w = new Worker();
        try{
            w.initialize();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    private void init(){

        String url = "jdbc:mysql://83.212.117.76:3306/ds_systems_2016";
        String username = "omada15";
        String password = "omada15db";
        System.out.println("Connecting to database...");
        /**
         * Try-catch block for connection & actions after the connection is made.
         * @param rs is the {@code ResultSet} returned from the database query that we iterate later
         * @param POI is the {@code String} regarding the POI ID
         * @param count is the {@code int} regarding the check in counter of that specific POI
         * @throws IllegalStateException if the database credentials are wrong
         */
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connected to database as: " + username);
            Statement stmt = connection.createStatement();
            System.out.println("Executing query: " + "SELECT POI, POI_name as name, POI_category as cat, latitude, longitude, photos FROM checkins");
            ResultSet rs = stmt.executeQuery("SELECT POI, POI_name as name, POI_category as cat, latitude, longitude, time, photos FROM checkins");
            System.out.println("Query retrieved");
            buffer = new ArrayList<>();
            while (rs.next()) {
                Date date = null;
                if (rs.getTimestamp("time") != null)
                    date = new java.util.Date(rs.getTimestamp("time").getTime());
                POIs tempO = new POIs(rs.getString("POI"), rs.getString("name"), rs.getString("cat"), rs.getDouble("latitude"), rs.getDouble("longitude"), 1, date);
                buffer.add(new Checkins(tempO,new ArrayList<>(Arrays.asList(rs.getString("photos")))));
            }
            rs.close();
            stmt.close();
            connection.close();
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
    }
    private void initialize() throws IOException{
        init();
        int port = 4321;
        int portR = 2485;
        ServerSocket[] ss = new ServerSocket[3];
        ServerSocket ssW = new ServerSocket(port-100);
        ServerSocket ssR = null;
        ss[0] = new ServerSocket(port);
        ss[1] = new ServerSocket(port+1);
        ss[2] = new ServerSocket(port+2);
        for (;;) {
            Socket clientSocket = null;
            Socket wSocket = null;
            tempBuffer = null;
            tempBuffer = new ArrayList<>();
            for(Checkins ch : buffer){
                try {
                    tempBuffer.add((Checkins) ch.clone());
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
            wSocket = ssW.accept();
            ObjectInputStream in = new ObjectInputStream(wSocket.getInputStream());
            int rand = in.readInt();
            int moreW = in.readInt();
            if(moreW==0){
                clientSocket = ss[0].accept();
                new Thread(new MapWorker(clientSocket, 1, tempBuffer, rand)).start();
            } else if(moreW==1)
                ssR = new ServerSocket(rand);
                new Thread(new ReduceWorker(ssR,rand)).start();

        }
    }

    public void waitForTasksThread(){

    }
}
